#!/bin/bash
# VOIR https://slurm.schedmd.com/sbatch.html
#SBATCH --time=3
#SBATCH --job-name=multi_core_test
#SBATCH --mem=4G
#SBATCH --cpus-per-task=6
#SBATCH --part=batch
unset XDG_RUNTIME_DIR

# Disable variable export with sbatch
export SBATCH_EXPORT=NONE

# Create user pip install folder

module load python/3.7.4 scipy-stack/2019b

${HOME}/projects/166600078/${USER}/intro_hpc/multi_core_load.py

