# Introduction bash + HPC    
    
J'ai nommé cette introduction: "mais il a pris le mors au dent!"    
    
## Prérequis    
    
Un compte VALERIA, chercheur ou collaborateur    
Un terminal ou un outil qui permet d'établir une connexion ssh. (https://mobaxterm.mobatek.net/download.html)    
Un peu de patience et de la curiosité    
    
## GO!    
    
### Se connecter sur le noeud externe, alias "login node" (2 min)    
    
Avec moba, faites "sessions/SSH", entrer les champs suivants puis faire OK:    
- remotehost: login.valeria.science    
- Specify username: VOTRE_IDUL    
   
> VOIR: https://mobaxterm.mobatek.net/demo.html    
    
À partir d'une ligne de commande linux/mac    
    
`ssh VOTRE_IDUL@login.valeria.science`   
    
!!! BRAVO, vous êtes sur une console (alias shell) linux, plus précisement le shell "bash".    
    
### bash 101 (5 min)    
    
La console permet d'accéder aux entrailles d'un système d'exploitation sans utiliser la souris ou de représentation graphique. Sous linux, le système de fichier et la console sont particulièrement puissant car il donne un accès très poussés au système d'exploitation. Cela est aussi plus performant dans le cas d'un serveur puisque l'environnement graphique n'est pas installé sur ceux-ci. Pour contrôler une console, on doit lui transmettre des commandes qu'elle connaît. Par exemple pour bash il y a les commandes suivantes:    
   
Commande    | Description    
----------- | -----------    
pwd			| voir le dossier dans lequel on se trouve    
ls 			| pour lister les fichiers et les dossiers    
cd 			| pour changer de dossier    
mkdir 		| pour créer un dossier    
touch 		| pour créer un fichier vide    
cat ou less	| pour afficher le contenu d'un fichier.    
nano ou vi 	| pour éditer un fichier    
    
> NOTE: la plupart des commandes utilisent des paramètres pour modifier leur comportement. La liste de ces paramètres peut être consulté grâce à un paramètre! --help    
    
```    
pwd    
ls    
ls --help # SHIFT + PAGE UP pour remonter dans l'historique de la console    
ls -l    
cd projects/    
ls -l    
cd 166600078 # C'est le projet de test pour VALERIA!    
mkdir VOTRE_IDUL # pour créer votre dossier perso    
cd VOTRE_IDUL    
touch premier_fichier    
ls -l    
```    
    
La commande `ls -l` donne:    
    
```    
[pylan1@ulaval.ca@ul-val-pr-ssh01 monprojet123]$ ls -l    
total 1    
-rw-rw-r-- 1 pylan1@ulaval.ca ul-val-prj-valeria-exp01@valeria.science 0 13 mai 14:31 premier_fichier    
```    
    
Décomposé, ça donne:    
Colonne                                     | Description    
------------------------------------------- | -----------    
-rw-rw-r-- 									| Les permissions POSIX: r: read, w: write, x: execute   
1 											| je sais pas!    
pylan1@ulaval.ca 							| le nom du propriétaire    
ul-val-prj-valeria-exp01@valeria.science 	| le nom du groupe d'accès    
0 											| la taille en octet    
13 mai 14:31 								| la date de création    
mon_premier_fichier							| le nom du fichier!    
    
> Poser des questions aux linuxeux pour plus d'info. Ils ne mordent pas et c'est dans leur nature de partager!    
> https://en.wikipedia.org/wiki/File_system_permissions#Numeric_notation    
    
### bash 102 (10 min)    
    
Bravo, bash 101 complété! Nous allons éditer votre premier_fichier. Pour cela utilisez la commande de votre choix. Nous allons y aller avec "nano".    
    
`nano premier_fichier`    
    
Vous pouvez maintenant tapez du contenu dans votre fichier! Ajoutez le contenu suivant:    
    
```    
Bonjour à tous    
Vive la canadienne    
Et pourquoi pas Gilles!    
```    
    
Pour sauvegarder et quitter, appuyer sur CTRL+X. Répondre en tapant sur la touche O puis ENTRER.    
    
Pour voir le contenu sans l'éditer faire la commande `cat premier_fichier`. La console bash permet de transformer le résultat de n'importe quelle commande en le redirigeant vers une autre commande. Il s'agit du principe "pipe and filter". Pour y arriver, on ajoute une barre verticale | nommé pipe.    
    
```   
wc --help # permet de compter plein de trucs, dont des lignes!    
cat premier_fichier | wc -l    
grep --help | head    
cat premier_fichier | grep our    
cat premier_fichier | grep can    
cat premier_fichier | grep -E 'can|ous'    
    
dmesg    
dmesg | grep mount # Cela permet de chercher pour des ERROR ou autre pattern dans les fichiers LOG.    
```    
  
Next step: l'éxécution d'une commande DANS une commande. (pitchoooouuuuu!)    
    
```    
date    
echo "Bonjour il est 12:12" # Heu non il est pas 12:12    
echo "Bonjour il est $(date)"    
echo "Bonjour il est $(ls)"    
echo "Bonjour il est $(cat premier_fichier)"    
echo "Bonjour il est $(cat premier_fichier|grep can)"    
```    

Mot de la fin de l'intro: les variables d'environnements!
```
echo "Bonjour $USER"
echo "Ta maison est ici: $HOME"
echo "Ta maison est ici: ${HOME}"... Ça donne la même chose, écriture différente!
env
```
   
> Cherchez sur le net pour plus d'infos!    
> https://www.howtogeek.com/412055/37-important-linux-commands-you-should-know/    
    
### Passons aux choses sérieuses (5 min)    
   
Voici une liste de commandes utiles    
   
Commande                                           | Description   
-------------------------------------------------- | -----------    
id                                                 | Donne l'identité de l'utilisateur actuel, avec les groupes dont il fait partie.    
id -u                                              | Ne donne que l'identifiant numérique (UID) de l'utilisateur. Ce UID est utilisé pour créer les quotas LUSTRE    
lfs quota -hp $(id -u) /lustre-a                   | Permet d'obtenir l'état de son quota utilisateur. Whoa, une commande incluse! pitchoooooou!    
getent passwd pylan1                               | Voir l'identité d'un utilisateur, un peu équivalent à `id -u`   
getent group ul-val-prj-valeria-exp01              | Voir les membres d'un groupe donné mais aussi son ID (GID): 166600078   
lfs quota -hp 166600078 /lustre-a                  | Pour voir le quota LUSTRE d'un dossier projet donné   
rm                                                 | Pour supprimer un fichier ou un dossier.    
htop                                               | Voir l'utilisation du serveur  
  
### PAUSE!  
   
### Slurm (20 min) 
  
SLURM (Simple Linux Utility for Resource Management) est une solution open source d'ordonnancement de tâches informatiques qui permet de créer des grappes de serveurs sous Linux ayant une tolérance aux pannes, type ip-failover, ferme de calcul, système d'ordonnancement des tâches.  
https://fr.wikipedia.org/wiki/SLURM  
  
Donc si j'ai besoin de faire un calcul sur 40 coeurs avec 2 GB RAM par coeur, et que j'ai une grappe de 200 serveurs, je ne veux pas me connecter sur chacun d'eux pour vérifier si le CPU et la RAM sont utilisés à 100% jusqu'à ce que j'en trouve un de libre... Pour ça, ya SLURM!  
  
Vous êtes présentement connecté sur la login node. Ce serveur ne sert que de pont vers l'internet. Il n'a pas de capacité de calcul. Si on veux exécuter un programme il faut passer par des commandes spécifiques à SLURM. En voici un sous ensemble:  
  
Commande                                               | Description   
------------------------------------------------------ | -----------
squeue                                                 | Affiche la liste des job slurm en cours d'exécution.    
sinfo                                                  | Affiche tous les serveurs et partitions gérés par SLURM et leur status (UP, DOWN, idle, mix, etc).    
salloc                                                 | Permet d'obtenir une session interactive sur l'un des noeuds.    
sbatch                                                 | Permet de lancer un script dans une session "non-interactive". Note file d'attente...
scancel                                                | Permet d'annuler une job slurm  
watch -n1 'systemd-cgtop -p | grep job_${SLURM_JOBID}' | Pour voir la mémoire utilisée par un processus. On doit être sur le bon noeud!!!

  
Ex: avec watch   
```  
watch -n 5 "squeue"  
```  

Faire chauffer la stack VALERIA!!!

```  
mkdir -p ${HOME}/projects/166600078/${USER}
cd ${HOME}/projects/166600078/${USER}
git clone https://pylan1@bitbucket.org/pylan1/intro_hpc.git
cd ${HOME}/projects/166600078/${USER}/intro_hpc
cat introduction_hpc.md
```  

Vous avez utilisé GIT! :D
À mon signal!

```  
ls -la
cat multi_core_load.py
cat submit_script.sh
sbatch submit_script.sh
cat slurm-XXXXX.out
# Demo avec autre partition / bigmem
```  
    
### Ceph + rclone (si on a le temps)
  
```  
mkdir -p ${HOME}/projects/166600078/${USER}/test_s3
cd ${HOME}/projects/166600078/${USER}/test_s3
```  

Un boucle d'exécution avec bash!

```  
echo "BONJOUR"
for i in {1..10}; do  echo "BONJOUR"; done
```  

Générer des fichiers random et GROS!
```  
mkdir ./data_$USER
dd if=/dev/urandom of=./data_$USER/mon_fichier.rnd count=1 bs=1024
ls ./data_$USER -lh
dd if=/dev/urandom of=./data_$USER/mon_fichier.rnd count=1024 bs=1024
dd if=/dev/urandom of=./data_$USER/mon_fichier.rnd count=100024 bs=1024
dd if=/dev/urandom of=./data_$USER/mon_fichier.rnd count=1000024 bs=1024
for i in {1..10}; do  dd if=/dev/urandom of=./data_$USER/mon_fichier_$i.rnd count=100024 bs=1024; done
```  
RCLONE! HO YHEA

```  
export MYBUCKET=new-bucket-db8eebbe
rclone config
rclone ls S3VAL:$MYBUCKET
rclone -vvv sync ./data_$USER/ S3VAL:$MYBUCKET/data_$USER
rclone -vvv --transfers 100 sync ./data_$USER/ S3VAL:$MYBUCKET/data_$USER
```  